#ifndef PIXELAPIABSTRACTION_H
#define PIXELAPIABSTRACTION_H

#include "CPURand.h"
#include "PixelCPUInfoHelper.h"
#include "RDRANDRand.h"
#include "TPMRand.h"
#include <QList>
#include <QSslCertificate>
#include <QSslConfiguration>
#include <QtWidgets/QWidget>
#include <immintrin.h>
#include <random>
#ifdef _MSC_VER
#include <Windows.h>
#include <dbt.h>
#include <intrin.h>
#include <wincrypt.h>

typedef int CPUINFO_INT;
#endif
#if defined(__linux__) || defined(__MINGW64__)
#include <cpuid.h>
#include <libusb.h>
typedef unsigned int CPUINFO_INT;
#endif

#ifdef _MSC_VER
struct PixelUSBHandlerMessage {
  MSG *msg;
};
#endif
#if defined(__linux__) || defined(__MINGW64__)
struct PixelUSBHandlerMessage {
  struct libusb_context *ctx;
  struct libusb_device *device;
  libusb_hotplug_event event;
  void *user_data;
}
#endif
class PixelAPISecAbstraction {
public:
  PixelAPISecAbstraction();
  void GetRANDUShort(std::vector<unsigned short> *in);

  // cross platform extended cpuinfo command
  static void GetPixelCPUID(PixelCPUInfo *cpuidcommand);

  static QList<QSslCertificate> GetSystemCertificates();
  static const unsigned char LEVELRDRAND = 2;
  static const unsigned char LEVELTPM = 1;
  static const unsigned char LEVELCPU = 0;

private:
  void InitialiseEngine(unsigned char level);
  char rdlevel;
  RandEngine *randengine;
  QSslConfiguration sslconfiguration;
};

// usbhotplugWindow for windows uses a window to listen to usb events, however
// in linux it uses libusb callback handle, so make usbhotplugWindow extend
// both
class USBHotPlugWindow : public QWidget {
public:
  USBHotPlugWindow();
  ~USBHotPlugWindow();
#ifdef _MSC_VER
  bool nativeEvent(
      const QByteArray &eventType, void *message,
      qintptr *result); // Windows event handler, pass to usbhandler function
#endif
#if defined(__linux__) || defined(__MINGW64__)
  libusb_hotplug_callback_fn LinuxCallback(
      libusb_context *ctx, libusb_device *device, libusb_hotplug_event event,
      void *user_data); // Linux event handler, pass to usbhandler function
#endif
  virtual int USBHandler(PixelUSBHandlerMessage *message);

private:
  PixelUSBHandlerMessage messagebuffer;

#ifdef _MSC_VER
  HDEVNOTIFY m_hdevicenotify;
#endif
#if defined(__linux__) || defined(__MINGW64__)
  libusb_hotplug_callback_handle hp;
#endif
};
#endif // PIXELAPIABSTRACTION_H
