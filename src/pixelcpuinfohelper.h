#ifndef PIXELCPUINFOHELPER_H
#define PIXELCPUINFOHELPER_H

#ifdef _MSC_VER
#include <intrin.h>
typedef int CPUINFO_INT;
#endif
#if defined(__linux__) || defined(__MINGW64__)
#include <cpuid.h>
typedef unsigned int CPUINFO_INT;
#endif

struct PixelCPUInfo {
  CPUINFO_INT cpuInfo[4];   // cpuInfo is the order EAX,EBX,ECX,EDX
  CPUINFO_INT function = 0; // function to pass into cpuid
  CPUINFO_INT subleaf = 0;  // subleaf to pass into cpuid
};

class PixelCPUInfoHelper {

public:
  void resetvalues();
  void executeCPUID();
  CPUINFO_INT geteax();
  CPUINFO_INT getebx();
  CPUINFO_INT getecx();
  CPUINFO_INT getedx();
  CPUINFO_INT getfuncvalue();
  CPUINFO_INT getsubleafvalue();
  void seteax(const CPUINFO_INT &in);
  void setebx(const CPUINFO_INT &in);
  void setecx(const CPUINFO_INT &in);
  void setedx(const CPUINFO_INT &in);
  void setfuncvalue(const CPUINFO_INT &in);
  void setsubleafvalue(const CPUINFO_INT &in);

private:
  PixelCPUInfo pixelcpuinfo;
};

#endif // PIXELCPUINFOHELPER_H
