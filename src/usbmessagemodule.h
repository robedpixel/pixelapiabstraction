#ifndef USBMESSAGEWINDOW_H
#define USBMESSAGEWINDOW_H
#include <QMainWindow>
#include <Windows.h>
#include <strsafe.h>
#include <dbt.h>
#include <Ntddstor.h>
#include <Rpc.h>
#include <QDebug>
#include <string>
#include <QFile>
#include <QString>
#if defined(Q_OS_LINUX)
    #include <libusb.h>
#elif defined(Q_OS_WIN)
    #include <winusb.h>
    #include <SetupAPI.h>
    #include <Windows.h>
    #include <stdio.h>
    #include <tchar.h>
    #include <strsafe.h>
    #include <dbt.h>
#else
#error "OS not supported!"
#endif

class USBMessageModule : public QMainWindow
{
    Q_OBJECT
public:
    USBMessageModule();
    ~USBMessageModule();

    #if defined(Q_OS_LINUX)
    libusb_hotplug_callback_fn callbackfn;
    int rc;
    int hotplug_callback(struct libusb_context *ctx, struct libusb_device *device,libusb_hotplug_event event, void *user_data)
    #elif defined(Q_OS_WIN)
    bool nativeEvent(const QByteArray &eventType, void *message, long *result);
    #else
    #error "OS not supported!"
    #endif
private:
    const unsigned short m_certifiedusbpid[1]={0};
    const unsigned short m_certifiedusbvid[1]={0};

    //platform specific declarations
#if defined(Q_OS_LINUX)

#elif defined(Q_OS_WIN)
    std::list<HANDLE> usblist;
    const char* maskarray="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    std::string deviceletter;
    HDEVNOTIFY m_hdevicenotify;
    DWORD m_devicetype;
    DWORD m_rawdrivelocation;
    unsigned int convertedbitnumber;
#else
#error "OS not supported!"
#endif

};

#endif // USBMESSAGEWINDOW_H
