#ifndef CPURAND_H
#define CPURAND_H

#include "RandEngine.h"
#include <random>

// RandEngine class using CPU
class CPURand : public RandEngine {
public:
  CPURand();
  ~CPURand();
  bool GetStatus();
  unsigned int GetRandUShorts(std::vector<uint16_t> *in);
  unsigned int GetRandUInts(std::vector<uint32_t> *in);
  unsigned int GetRandULongs(std::vector<uint64_t> *in);

private:
  using RandEngine::GetRandUInts;
  using RandEngine::GetRandULongs;
  using RandEngine::GetRandUShorts;
  using RandEngine::GetStatus;
  std::independent_bits_engine<std::default_random_engine, 16, uint16_t>
      *cpuengine;
  std::vector<uint16_t> buffer;
  std::vector<uint16_t> longbuffer;
};

#endif // CPURAND_H
