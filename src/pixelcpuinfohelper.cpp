#include "PixelCPUInfoHelper.h"

void PixelCPUInfoHelper::resetvalues() {
  for (int i = 0; i < 4; i++) {
    pixelcpuinfo.cpuInfo[i] = 0;
  }
  pixelcpuinfo.function = 0;
  pixelcpuinfo.subleaf = 0;
}
void PixelCPUInfoHelper::executeCPUID() {
#ifdef _MSC_VER
  __cpuidex(pixelcpuinfo.cpuInfo, pixelcpuinfo.function, pixelcpuinfo.subleaf);
#endif
#if defined(__linux__) || defined(__MINGW64__)
  __get_cpuid_count(pixelcpuinfo.function, pixelcpuinfo.subleaf,
                    pixelcpuinfo.cpuInfo[0], pixelcpuinfo.cpuInfo[1],
                    pixelcpuinfo.cpuInfo[2], pixelcpuinfo.cpuInfo[3]);
#endif
}
CPUINFO_INT PixelCPUInfoHelper::geteax() { return pixelcpuinfo.cpuInfo[0]; }
CPUINFO_INT PixelCPUInfoHelper::getebx() { return pixelcpuinfo.cpuInfo[1]; }
CPUINFO_INT PixelCPUInfoHelper::getecx() { return pixelcpuinfo.cpuInfo[2]; }
CPUINFO_INT PixelCPUInfoHelper::getedx() { return pixelcpuinfo.cpuInfo[3]; }
CPUINFO_INT PixelCPUInfoHelper::getfuncvalue() { return pixelcpuinfo.function; }
CPUINFO_INT PixelCPUInfoHelper::getsubleafvalue() {
  return pixelcpuinfo.subleaf;
}
void PixelCPUInfoHelper::seteax(const CPUINFO_INT &in) {
  pixelcpuinfo.cpuInfo[0] = in;
}
void PixelCPUInfoHelper::setebx(const CPUINFO_INT &in) {
  pixelcpuinfo.cpuInfo[1] = in;
}
void PixelCPUInfoHelper::setecx(const CPUINFO_INT &in) {
  pixelcpuinfo.cpuInfo[2] = in;
}
void PixelCPUInfoHelper::setedx(const CPUINFO_INT &in) {
  pixelcpuinfo.cpuInfo[3] = in;
}
void PixelCPUInfoHelper::setfuncvalue(const CPUINFO_INT &in) {
  pixelcpuinfo.function = in;
}
void PixelCPUInfoHelper::setsubleafvalue(const CPUINFO_INT &in) {
  pixelcpuinfo.subleaf = in;
}
