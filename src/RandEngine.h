// Fill out your copyright notice in the Description page of Project Settings.

#ifndef RANDENGINE_H
#define RANDENGINE_H

#include <vector>

// Virtual class for RNG types
class RandEngine {
public:
  virtual bool GetStatus();
  virtual unsigned int GetRandUShorts(std::vector<uint16_t> *in);
  virtual unsigned int GetRandUInts(std::vector<uint32_t> *in);
  virtual unsigned int GetRandULongs(std::vector<uint64_t> *in);
};

#endif // RANDENGINE_H
