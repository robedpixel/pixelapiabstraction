#include "pixelapiabstraction.h"

PixelAPISecAbstraction::PixelAPISecAbstraction() {
  rdlevel = 0;
#ifdef _MSC_VER
  int cpuinfo[4];
  __cpuidex(cpuinfo, 1, 0);
  if ((cpuinfo[2] & 0x40000000) == 0x40000000) {
    rdlevel = LEVELRDRAND;
  }
#elif defined(__GNUG__)
  unsigned int info[4];
  __get_cpuid_count(0, 1, &(info[0]), &(info[1]), &(info[2]), &(info[3]));
  if ((info[2] & 0x40000000) == 0x40000000) {
    rdlevel = LEVELRDRAND;
  }
#endif
  if (rdlevel != LEVELRDRAND) {
    TSS2_RC tpmerr;
    ESYS_CONTEXT *tpmcontext;
    tpmerr = Esys_Initialize(&tpmcontext, nullptr, nullptr);
    if (tpmerr == TSS2_RC_SUCCESS) {
      rdlevel = LEVELTPM;
    }
    Esys_Finalize(&tpmcontext);
  }
  InitialiseEngine(rdlevel);
}
void PixelAPISecAbstraction::InitialiseEngine(unsigned char level) {
  switch (level) {
  case LEVELRDRAND:
    randengine = new RDRANDRand();
    break;
  case LEVELTPM:
    randengine = new TPMRand();
    break;
  case LEVELCPU:
    randengine = new CPURand();
    break;
  }
}
void PixelAPISecAbstraction::GetRANDUShort(std::vector<unsigned short> *in) {
  randengine->GetRandUShorts(in);
}

void PixelAPISecAbstraction::GetPixelCPUID(PixelCPUInfo *cpuidcommand) {
#ifdef _MSC_VER
  __cpuidex(cpuidcommand->cpuInfo, cpuidcommand->function,
            cpuidcommand->subleaf);
#endif
#if defined(__linux__) || defined(__MINGW64__)
  __get_cpuid_count(cpuidcommand->function, cpuidcommand->subleaf,
                    &(cpuidcommand->cpuInfo[0]), &(cpuidcommand->cpuInfo[1]),
                    &(cpuidcommand->cpuInfo[2]), &(cpuidcommand->cpuInfo[3]));
#endif
}
QList<QSslCertificate> PixelAPISecAbstraction::GetSystemCertificates() {
  return QSslConfiguration::defaultConfiguration().caCertificates();
}
USBHotPlugWindow::USBHotPlugWindow() {
#ifdef _MSC_VER
  // register callback for windows
  DEV_BROADCAST_DEVICEINTERFACE devInt;
  ZeroMemory(&devInt, sizeof(devInt));
  devInt.dbcc_size = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
  devInt.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
  devInt.dbcc_classguid = GUID_DEVINTERFACE_VOLUME;
  m_hdevicenotify = RegisterDeviceNotification(
      (HANDLE)effectiveWinId(), &devInt, DEVICE_NOTIFY_WINDOW_HANDLE);
#endif
#if defined(__linux__) || defined(__MINGW64__)
  // register callback for linux
  int rc;
  rc = libusb_init(NULL);
  if (rc < 0) {
          qDebug() << "failed to initialise libusb: " << libusb_error_name(rc));
          return 1;
  }
  if (!libusb_has_capability(LIBUSB_CAP_HAS_HOTPLUG)) {
    qDebug() << "Hotplug capabilites are not supported on this platform";
    libusb_exit(NULL);
    return;
  }
  rc = libusb_hotplug_register_callback(
      NULL, LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED, 0, vendor_id, product_id,
      class_id, hotplug_callback, NULL, &hp);
  if (LIBUSB_SUCCESS != rc) {
    qDebug() << "Error registering callback";
    libusb_exit(NULL);
    return;
  }
#endif
}
USBHotPlugWindow::~USBHotPlugWindow() {
  qDebug() << "shutting down hotplug listener";

#ifdef _MSC_VER
  // deregister callback
  UnregisterDeviceNotification(m_hdevicenotify);
#endif
#if defined(__linux__) || defined(__MINGW64__)
  libusb_hotplug_deregister_callback(NULL, hp);
  libusb_exit(NULL);
#endif
}
#ifdef _MSC_VER
bool USBHotPlugWindow::nativeEvent(const QByteArray &eventType, void *message,
                                   qintptr *result) {
  MSG *msg = static_cast<MSG *>(message);
  messagebuffer.msg = msg;
  this->USBHandler(&messagebuffer);
  return false;
}
#endif

#if defined(__linux__) || defined(__MINGW64__)
USBHotPlugWindow::LinuxCallback(libusb_context *ctx, libusb_device *device,
                                libusb_hotplug_event event, void *user_data) {
  messagebuffer.ctx = ctx;
  messagebuffer.device = device;
  messagebuffer.event = event;
  messagebuffer.user_data = user_data;
  this->USBHandler(&messagebuffer);
  return 0;
}
#endif

int USBHotPlugWindow::USBHandler(PixelUSBHandlerMessage *message) { return 0; }
