// Fill out your copyright notice in the Description page of Project Settings.

#ifndef RDRANDRAND_H
#define RDRANDRAND_H

#include "PixelCPUInfoHelper.h"
#include "RandEngine.h"
#include <intrin.h>

// TRNG RandEngine class using RDRAND
class RDRANDRand : public RandEngine {

public:
  RDRANDRand();
  bool GetStatus();
  unsigned int GetRandUShorts(std::vector<uint16_t> *in);
  unsigned int GetRandUInts(std::vector<uint32_t> *in);
  unsigned int GetRandULongs(std::vector<uint64_t> *in);

private:
  bool enginestatus = false;
  using RandEngine::GetRandUInts;
  using RandEngine::GetRandULongs;
  using RandEngine::GetRandUShorts;
  using RandEngine::GetStatus;
};

#endif // RDRANDRAND_H
