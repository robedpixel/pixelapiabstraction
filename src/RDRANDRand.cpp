// Fill out your copyright notice in the Description page of Project Settings.

#include "RDRANDRand.h"
RDRANDRand::RDRANDRand() {
  PixelCPUInfoHelper info;
  info.resetvalues();
  info.setfuncvalue(1);
  info.setsubleafvalue(0);
  info.executeCPUID();
  if ((info.getecx() & 0x40000000) == 0x40000000) {
    enginestatus = true;
  }
}
bool RDRANDRand::GetStatus() { return enginestatus; }
unsigned int RDRANDRand::GetRandUShorts(std::vector<uint16_t> *in) {
  for (unsigned int i = 0; i < in->size(); i++) {
    _rdrand16_step(&(in->operator[](i)));
  }
  return 0;
}
unsigned int RDRANDRand::GetRandUInts(std::vector<uint32_t> *in) {
  for (unsigned int i = 0; i < in->size(); i++) {
    _rdrand32_step(&(in->operator[](i)));
  }
  return 0;
}
unsigned int RDRANDRand::GetRandULongs(std::vector<uint64_t> *in) {
  for (unsigned int i = 0; i < in->size(); i++) {
    _rdrand64_step(&(in->operator[](i)));
  }
  return 0;
}
