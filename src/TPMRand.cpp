// Fill out your copyright notice in the Description page of Project Settings.

#include "TPMRand.h"
// TEST COMMENT
TPMRand::TPMRand() {
  tpmerr = Esys_Initialize(&tpmcontext, nullptr, nullptr);
  if (tpmerr == TSS2_RC_SUCCESS) {
    enginestatus = true;
  }
}
TPMRand::~TPMRand() {
  if (tpmcontext) {
    Esys_Finalize(&tpmcontext);
  }
}
bool TPMRand::GetStatus() { return enginestatus; }
unsigned int TPMRand::GetRandUShorts(std::vector<uint16_t> *in) {
  numbytesgen = in->size() * 2;
  // generate random numbers from tpm until buffer is filled
  if (numbytesgen <= maxbytesize) {
    Esys_GetRandom(tpmcontext, ESYS_TR_NONE, ESYS_TR_NONE, ESYS_TR_NONE,
                   numbytesgen, &randombytes);
    for (unsigned int i = 0; i < in->size(); i++) {
      in->operator[](i) = static_cast<unsigned int>(
          randombytes->buffer[2 * i] << 8 | randombytes->buffer[2 * i + 1]);
    }
    free(randombytes);
    return 0;
  } else {
    return 1;
  }
}

unsigned int TPMRand::GetRandUInts(std::vector<uint32_t> *in) {
  numbytesgen = in->size() * 4;
  if (numbytesgen <= maxbytesize) {
    Esys_GetRandom(tpmcontext, ESYS_TR_NONE, ESYS_TR_NONE, ESYS_TR_NONE,
                   numbytesgen, &randombytes);
    for (unsigned int i = 0; i < in->size(); i++) {
      in->operator[](i) = static_cast<unsigned int>(
          static_cast<uint32_t>(randombytes->buffer[4 * i]) << 24 |
          static_cast<uint32_t>(randombytes->buffer[4 * i + 1]) << 16 |
          static_cast<uint32_t>(randombytes->buffer[4 * i + 2]) << 8 |
          static_cast<uint32_t>(randombytes->buffer[4 * i + 3]));
    }
    free(randombytes);
    return 0;
  } else {
    return 1;
  }
}
unsigned int TPMRand::GetRandULongs(std::vector<uint64_t> *in) {
  numbytesgen = in->size() * 8;
  if (numbytesgen <= maxbytesize) {
    Esys_GetRandom(tpmcontext, ESYS_TR_NONE, ESYS_TR_NONE, ESYS_TR_NONE,
                   numbytesgen, &randombytes);
    for (unsigned int i = 0; i < in->size(); i++) {
      in->operator[](i) = static_cast<uint64_t>(
          static_cast<uint64_t>(randombytes->buffer[8 * i]) << 56 |
          static_cast<uint64_t>(randombytes->buffer[8 * i + 1]) << 48 |
          static_cast<uint64_t>(randombytes->buffer[8 * i + 2]) << 40 |
          static_cast<uint64_t>(randombytes->buffer[8 * i + 3]) << 32 |
          static_cast<uint64_t>(randombytes->buffer[8 * i + 4]) << 24 |
          static_cast<uint64_t>(randombytes->buffer[8 * i + 5]) << 16 |
          static_cast<uint64_t>(randombytes->buffer[8 * i + 6]) << 8 |
          static_cast<uint64_t>(randombytes->buffer[8 * i + 7]));
    }
    free(randombytes);
    return 0;
  } else {
    return 1;
  }
}
