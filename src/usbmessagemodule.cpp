#include "usbmessagemodule.h"

USBMessageModule::USBMessageModule()
{
    #if defined(Q_OS_LINUX)
    callbackfn = std::mem_fn(&USBMessageModule::hotplug_callback)
    rc = libusb_hotplug_register_callback(NULL, LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED
                                          , 0, 0x045a, 0x5005,
                                            LIBUSB_HOTPLUG_MATCH_ANY, callbackfn, NULL,
                                            &handle);
    #elif defined(Q_OS_WIN)

        DEV_BROADCAST_DEVICEINTERFACE devInt;
        ZeroMemory( &devInt, sizeof(devInt) );
        devInt.dbcc_size = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
        devInt.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
        devInt.dbcc_classguid = GUID_DEVINTERFACE_VOLUME;
        m_hdevicenotify = RegisterDeviceNotification( (HANDLE)effectiveWinId(), &devInt, DEVICE_NOTIFY_WINDOW_HANDLE );
    #else
    #error "OS not supported!"
    #endif

}
USBMessageModule::~USBMessageModule(){

    qDebug()<<"shutting down messagemodule";


    #if defined(Q_OS_LINUX)
    libusb_hotplug_deregister_callback(NULL, handle)
    #elif defined(Q_OS_WIN)
    UnregisterDeviceNotification(m_hdevicenotify);
    for (std::list<HANDLE>::iterator it = usblist.begin(); it != usblist.end(); ++it){
        CloseHandle(*it);
    }
    #else
    #error "OS not supported!"
    #endif
}


#if defined(Q_OS_LINUX)
int USBMessageModule::hotplug_callback(struct libusb_context *ctx, struct libusb_device *device,libusb_hotplug_event event, void *user_data){
    qDebug()<<"Device Detected!";
    //check if device is a storage drive

    //check device vid and pid
    if ((device->descriptor.idVendor==m_certifiedusbvid[0])&&(device->descriptor.idProduct==m_certifiedusbpid[0])){
        //check for hidden identifier file
        QString hfilename = deviceletter+":/"+".uidentifier";
        QFile hfile(hfilename);
        if (hfile.exists()==true){
            //create keyfile

            //decrypt
            bool p_a= process.startDetached("C:/Program Files/VeraCrypt/VeraCrypt.exe",QStringList()<<"/v"<<);
            process.waitForFinished();
            qDebug()<<p_a;
        }else{
            qDebug()<<"non-certified volume detected!";
        }   
    }
}
#elif defined(Q_OS_WIN)
bool USBMessageModule::nativeEvent(const QByteArray &eventType, void *message, long *result){
    Q_UNUSED(result);
    Q_UNUSED(eventType);
    MSG* msg = static_cast<MSG*>(message);
    switch(msg->message){

    case WM_DEVICECHANGE:
        qDebug()<<"Device Detected!";
        switch(msg->wParam)
        case DBT_DEVICEARRIVAL:
        {
            //check the identity of the attached device
            DEV_BROADCAST_HDR* tempvalue1 = (DEV_BROADCAST_HDR*)(msg->lParam);
            m_devicetype = tempvalue1->dbch_devicetype;
            if(m_devicetype==DBT_DEVTYP_DEVICEINTERFACE){
                qDebug()<<"Its a storage drive...I think.";
                DEV_BROADCAST_DEVICEINTERFACE* tempvalue2 = (DEV_BROADCAST_DEVICEINTERFACE*)(msg->lParam);
            }else if(m_devicetype==DBT_DEVTYP_VOLUME){
                qDebug()<<"Its a storage drive!";
                DEV_BROADCAST_VOLUME* tempvalue2 = (DEV_BROADCAST_VOLUME*)(msg->lParam);
                //get volume info, if name matches decrypt it//
                m_rawdrivelocation = tempvalue2->dbcv_unitmask;
                //get the drive number
                convertedbitnumber = m_rawdrivelocation;
                deviceletter = maskarray[convertedbitnumber];
                std::wstring usbguidstring;
                std::string converteddriveletter;
                converteddriveletter = deviceletter + ":\\" ;
                GetVolumeNameForVolumeMountPoint(&converteddriveletter,&usbguidstring,256);
                usblist.push_back(CreateFile(&usbguidstring,GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ | FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL));
                PWINUSB_INTERFACE_HANDLE usbhandle;
                WinUsb_Initialize(usblist.front(),usbhandle);
                USB_DEVICE_DESCRIPTOR udd;
                memset(&udd, 0, sizeof(udd));
                ULONG LengthTransferred = 0;
                WinUsb_GetDescriptor(usbhandle,
                                     USB_DEVICE_DESCRIPTOR_TYPE, // Daniel K's suggestion
                                     0,
                                     0x409,     // asks for English
                                     &udd,
                                     sizeof(udd),
                                     &LengthTransferred);
                for (int i=0;i<1;i++){
                    if (udd.idVendor==m_certifiedusbvid[i]){
                        for (int a=0;i<1;i++){
                            if(udd.idProduct==m_certifiedusbpid[a]){

                                //check for hidden identifier file
                                QString hfilename = deviceletter+":/"+".uidentifier";
                                QFile hfile(hfilename);
                                if (hfile.exists()==true){
                                    //create keyfile

                                    //decrypt
                                    bool p_a= process.startDetached("C:/Program Files/VeraCrypt/VeraCrypt.exe",QStringList()<<"/v"<<);
                                    process.waitForFinished();
                                    qDebug()<<p_a;
                                }else{
                                    qDebug()<<"non-certified volume detected!";
                                }
                                break;
                            }
                        }
                    }
                }
                WinUsb_Free(usbhandle);
            }else{                

                qDebug()<<"i don't know what this usb device is";
            }

        }
            break;
        case DBT_DEVICEREMOVECOMPLETE:
            break;
        case DBT_DEVNODES_CHANGED:
            break;

    }

    return false;
}
#else
#error "OS not supported!"
#endif
