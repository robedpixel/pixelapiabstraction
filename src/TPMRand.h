// Fill out your copyright notice in the Description page of Project Settings.

#ifndef TPMRAND_H
#define TPMRAND_H

#include "RandEngine.h"
#include <tss2/tss2_esys.h>
#include <tss2/tss2_tpm2_types.h>

// TRNG RandEngine class using TPM2
class TPMRand : public RandEngine {
public:
  TPMRand();
  ~TPMRand();
  bool GetStatus();
  unsigned int GetRandUShorts(std::vector<uint16_t> *in);
  unsigned int GetRandUInts(std::vector<uint32_t> *in);
  unsigned int GetRandULongs(std::vector<uint64_t> *in);

private:
  using RandEngine::GetRandUInts;
  using RandEngine::GetRandULongs;
  using RandEngine::GetRandUShorts;
  using RandEngine::GetStatus;
  bool enginestatus = false;
  const unsigned int maxbytesize = 65535;
  TSS2_RC tpmerr;
  TPM2B_DIGEST *randombytes = nullptr;
  ESYS_CONTEXT *tpmcontext = nullptr;
  unsigned int numbytesgen;
};

#endif // TPMRAND_H
