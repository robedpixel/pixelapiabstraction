#include "CPURand.h"

CPURand::CPURand() {
  cpuengine =
      new std::independent_bits_engine<std::default_random_engine, 16,
                                       uint16_t>(std::random_device{}());
  buffer.resize(2);
  longbuffer.resize(4);
}
CPURand::~CPURand() { delete cpuengine; }

bool CPURand::GetStatus() { return true; }
unsigned int CPURand::GetRandUShorts(std::vector<uint16_t> *in) {
  std::generate(in->begin(), in->end(), std::ref(*cpuengine));
  return 0;
}
unsigned int CPURand::GetRandUInts(std::vector<uint32_t> *in) {
  for (unsigned int i = 0; i < in->size(); i++) {
    std::generate(buffer.begin(), buffer.end(), std::ref(*cpuengine));
    in->operator[](i) =
        static_cast<uint32_t>(static_cast<uint32_t>(buffer[0]) << 16 |
                              static_cast<uint32_t>(buffer[1]));
  }
  return 0;
}
unsigned int CPURand::GetRandULongs(std::vector<uint64_t> *in) {
  for (unsigned int i = 0; i < in->size(); i++) {
    std::generate(longbuffer.begin(), longbuffer.end(), std::ref(*cpuengine));
    in->operator[](i) =
        static_cast<uint64_t>(static_cast<uint64_t>(buffer[0]) << 48 |
                              static_cast<uint64_t>(buffer[1]) << 32 |
                              static_cast<uint64_t>(buffer[2]) << 16 |
                              static_cast<uint64_t>(buffer[3]));
  }
  return 0;
}
